import { View, Text, StyleSheet } from 'react-native';
import React from 'react';
import AvatarComponent from '../AvatarComponent';
import { getInitials } from '../../utils/getInitials';
interface AuthUserComponentProps {
  imageUrl: string;
  userName: string;
  userPhone: string;
}

export default function AuthUserComponent({
  imageUrl,
  userName,
  userPhone,
}: AuthUserComponentProps) {
  const im =
    'https://www.shutterstock.com/shutterstock/photos/1865153395/display_1500/stock-photo-portrait-of-young-smiling-woman-looking-at-camera-with-crossed-arms-happy-girl-standing-in-1865153395.jpg';
  return (
    <View style={styles.authUserBlock}>
      <AvatarComponent initials={getInitials(userName)} imageUrl={imageUrl} />
      <View style={styles.usersData}>
        <Text style={styles.userName}>{userName}</Text>
        <Text style={styles.userPhone}>{userPhone}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  userPhone: {
    color: '#7F7F92',
    fontSize: 14,
  },
  userName: {
    color: '#1D1D1E',
    fontSize: 16,
    fontFamily: 'Inter-SemiBold',
    marginBottom: 6,
  },
  usersData: {
    alignSelf: 'center',
    marginLeft: 12,
  },
  authUserBlock: {
   
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 16,
  },
});
