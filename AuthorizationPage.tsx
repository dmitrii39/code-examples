import React, { useEffect, useState, useRef } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
} from 'react-native';
import Button from '../../components/Button';
import InputField from '../../components/InputField';
import { NavigationProps } from '../../navigation';
import { phoneNormalize } from '../../utils/phone';
import { convertPhoneNumber } from '../../utils/phone';
import { toFirebasePhoneNumber } from '../../utils/phone';
import { enterText } from '../../utils/texts';
import { initializeApp } from 'firebase/app';
import Student from '../../assets/icons/student.svg';
import OrangeArrow from '../../assets/icons/OrangeArrow.svg';
import CloseIcon from '../../assets/icons/close-icon.svg';
import Checkicon from '../../assets/icons/check.svg';
import RedCloseIcon from '../../assets/icons/red-close-icon.svg';
import GoBackArrow from '../../assets/icons/GoBackArrow.svg';
import {
  FirebaseRecaptchaVerifierModal,
  FirebaseRecaptchaBanner,
} from 'expo-firebase-recaptcha';
import {
  getAuth,
  PhoneAuthProvider,
} from 'firebase/auth';
import firebaseConfig from '../../../config/firebase';
import * as firebase from 'firebase/app';
import Checkbox from 'expo-checkbox';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
  toConfidentialityPolicyLink,
  whatsappUrlHandler,
} from '../../utils/links';
import { handleLink } from '../../utils/links';
import { TableContent } from '../../api/settings';
import api from '../../api';
import ProfileModalComponent from '../../components/ProfileModalComponent';
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
try {
  firebase.initializeApp(firebaseConfig);
} catch (error) {
  console.log('Initializing error ', error);
}

if (!app?.options || Platform.OS === 'web') {
  throw new Error(
    'This example only works on Android or iOS, and requires a valid Firebase config.'
  );
}

export default function Authorization({
  navigation,
}: NavigationProps<'Profile', 'Authorization'>) {
  const recaptchaVerifier = useRef(null);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [convertedPhoneNumber, setConvertedPhoneNumber] = useState('');
  const [firebasePhoneNumber, setFirebasePhoneNumber] = useState('');
  const [verificationId, setVerificationID] = useState('');
  // const [verificationCode, setVerificationCode] = useState("");

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isNumberExists, setIsNumberExists] = useState<boolean | undefined>(
    false
  );
  const fbConfig = app ? app.options : undefined;
  const [info, setInfo] = useState('');
  const attemptInvisibleVerification = false;
  const [isChecked, setIsChecked] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [pupils, setPupils] = useState<Array<TableContent['Ученики']>>();
  const [staffPhone, setStaffPhone] = useState<TableContent['Телефон'][]>([]);

  //------ввод и отправка номера телефона для получения СМС----------
  const handleSendVerificationCode = async () => {
    try {
      const phoneProvider = new PhoneAuthProvider(auth);
      const verificationId = await phoneProvider.verifyPhoneNumber(
        firebasePhoneNumber,
        recaptchaVerifier.current
      ); // get the verification id
      setVerificationID(verificationId);
      setInfo('Success : Verification code has been sent to your phone'); // If Ok, show message.
      // setPhoneNumber('')
      navigation.navigate('EnterSms', {
        firebasePhoneNumber: firebasePhoneNumber,
        phoneNumber: phoneNumber,
        verificationId: verificationId,
        info: info,
      });
    } catch (error) {
      setInfo(`Error : ${error}`); // show the error
    }
  };
  //-------------------------------------------------------

  const toConfidentialityPolicyLinkHandler = () => {
    // Обработчик нажатия на ссылку
    //  логикa для открытия страницы с информацией о персональных данных
    handleLink(toConfidentialityPolicyLink);
  };

  const openModal = () => {
    setModalVisible(true);
  };
  const closeModal = () => {
    setModalVisible(false);
  };
  const clearInput = () => {
    setPhoneNumber('');
    setConvertedPhoneNumber('');
    setIsNumberExists(false);
  };

  async function onLoad() {
    setPupils((await api.readTable('Ученики')) || undefined);
  }
  async function getStuffPhones() {
    try {
      const phones = (await api.readTable('Телефон')) || [];
      if (phones) {
        setStaffPhone(phones)
      }
    } catch (error) {
    }
  }

  const managersPhone = staffPhone[0]?.Телефон

  useEffect(() => {
    onLoad();
    getStuffPhones();
  }, []);

  //-----вывод массива с телефонами учеников из  Google таблицы ("Ученики")
  const phonesList = pupils?.map((item) => item.Телефон);
  //---------------------------------------------

  const handleCheckPhoneNumber = () => {
    setIsLoading(true);

    setTimeout(() => {
      const isNumberExists = phonesList?.includes(convertedPhoneNumber);
      setIsNumberExists(isNumberExists);
      setIsLoading(false);
    }, 2000);
  };

  useEffect(() => {
    if (convertedPhoneNumber.length === 11) {
      handleCheckPhoneNumber();
    }
  }, [convertedPhoneNumber]);

  const borderColorHandler = (): string => {
    if (convertedPhoneNumber.length === 11 && isLoading) {
      return '#1D1D1E';
    } else if (
      convertedPhoneNumber.length < 11 &&
      convertedPhoneNumber.length > 0
    ) {
      return '#1D1D1E';
    } else if (isNumberExists) {
      return '#F89235';
    } else if (!isNumberExists && convertedPhoneNumber.length === 11) {
      return '#E51717';
    } else {
      return '#fff';
    }
  };

  //-------------------------------------------
  return (
    <SafeAreaView style={styles.upperContainer}>
      <KeyboardAwareScrollView style={styles.keyBoardStyle}>
        <View style={styles.container}>
          <FirebaseRecaptchaVerifierModal
            ref={recaptchaVerifier}
            firebaseConfig={fbConfig}
          />
          <View style={styles.homeContent}>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={styles.goBack}
            >
              <GoBackArrow />
            </TouchableOpacity>
            <Text style={styles.header}>Вход</Text>
            <Text style={styles.enterText}>{enterText}</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <InputField
                borderWidth={1}
                borderColor={borderColorHandler()}
                type="phone-pad"
                maxLength={16}
                head="Номер телефона"
                placeholder="+7 911 511-22-33"
                value={phoneNumber}
                // info={info}

                onChangeValue={(phoneNumber) => {
                  setPhoneNumber(phoneNormalize(phoneNumber)),
                    setConvertedPhoneNumber(convertPhoneNumber(phoneNumber)),
                    setFirebasePhoneNumber(toFirebasePhoneNumber(phoneNumber));
                }}
              />
              {isLoading ? (
                <TouchableOpacity
                  onPress={clearInput}
                  style={styles.closeIconStyle}
                >
                  <CloseIcon />
                </TouchableOpacity>
              ) : convertedPhoneNumber.length < 11 &&
                convertedPhoneNumber.length > 0 ? (
                <TouchableOpacity
                  onPress={clearInput}
                  style={styles.closeIconStyle}
                >
                  <CloseIcon />
                </TouchableOpacity>
              ) : isNumberExists ? (
                <Checkicon style={styles.checkIconStyle} />
              ) : !isNumberExists && convertedPhoneNumber.length === 11 ? (
                <TouchableOpacity
                  onPress={clearInput}
                  style={styles.closeIconStyle}
                >
                  <RedCloseIcon />
                </TouchableOpacity>
              ) : null}
            </View>
              {!isLoading &&
              !isNumberExists &&
              convertedPhoneNumber.length === 11
                ? 
                (<Text style={styles.alertText}>Номер не зарегистрирован в базе центра</Text>)
                : null}
          </View>

          {isNumberExists && isChecked ? (
            <View style={styles.button}>
              <Button
                backgroundColor={'#E73F1A'}
                marginBottom={16}
                textColor="#fff"
                text="Продолжить"
                onPress={() => {
                  handleSendVerificationCode();
                }}
              />
            </View>
          ) : (
            <View style={{ paddingHorizontal: 6, marginTop: 20}}>
              <View style={styles.inActiveButton}>
                {isLoading ? (
                  <ActivityIndicator size="small" color="#FFFFFF" />
                ) : (
                  <Text style={styles.inActiveText}>Продолжить</Text>
                )}
              </View>
            </View>
          )}

          <View style={styles.checkBlock}>
            <Checkbox
              style={styles.checkbox}
              value={isChecked}
              hitSlop={{top: 44, bottom: 44, right: 44, left: 44}}
              onValueChange={() => setIsChecked(!isChecked)}
              color={isChecked ? '#7F7F92' : undefined}
            />
            <Text style={styles.checkBoxTextStyle}>
              Даю согласие на обработку моих{' '}
              <TouchableOpacity onPress={toConfidentialityPolicyLinkHandler}>
                <Text style={styles.subTextStyle}>персональных данных</Text>
              </TouchableOpacity>
            </Text>
          </View>
          <View style={styles.notYetPupilBlock}>
            <View style={styles.pupilBlock}>
              <Student />
              <Text style={styles.pupilText}>
                Ещё не являетесь учеником English Centre?
              </Text>
            </View>
            <TouchableOpacity onPress={openModal} style={styles.joinBlock}>
              <Text style={styles.joinText}>Присоединиться</Text>
              <View style={styles.OrangeArrowWrapper}>
                <OrangeArrow />
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.bottomStyle}></View>
          <ProfileModalComponent
            whatsAppOnPress={() => handleLink(whatsappUrlHandler(managersPhone))}
            onRequestClose={closeModal}
            onPress={closeModal}
            visible={modalVisible}
            />
        </View>

        {attemptInvisibleVerification && <FirebaseRecaptchaBanner />}
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  goBack: {
    marginTop: 34,
  },
  bottomStyle: {
    width: '100%',
    height: 80,
  },
  inActiveButton: {
    alignItems: 'center',
    padding: 19,
    marginBottom: 16,
    borderRadius: 26,
    backgroundColor: '#7F7F92',
  },
  inActiveText: {
    fontSize: 16,
    fontFamily: 'Inter-SemiBold',
    color: '#fff',
  },
  alertText: {
    color: '#E51717',
    fontSize: 14,
    fontFamily: 'Inter-Regular',
  },
  closeIconStyle: {
    position: 'absolute',
    top: 53,
    right: 30,
  },
  checkIconStyle: {
    position: 'absolute',
    top: 45,
    right: 23,
  },

  keyBoardStyle: {
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: 19,
    fontFamily: 'Inter-SemiBold',
    marginBottom: 14,
  },
  OrangeArrowWrapper: {
    alignSelf: 'center',
    marginLeft: 6,
  },
  joinText: {
    color: '#E73F1A',
    fontSize: 14,
    fontFamily: 'Inter-SemiBold',
    marginLeft: 55,
  },
  joinBlock: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  pupilText: {
    width: 200,
    fontSize: 14,
    color: '#1D1D1E',
    fontFamily: 'Inter-Regular',
    lineHeight: 20,
    alignSelf: 'center',
    marginLeft: 16,
  },
  pupilBlock: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 10,
  },

  notYetPupilBlock: {
    width: '98%',
    paddingTop: 20,
    paddingLeft: 20,
    paddingBottom: 20,
    backgroundColor: '#EAEAEA',
    borderRadius: 16,
    alignSelf: 'center',
  },
  checkBoxTextStyle: {
    color: '#7F7F92',
    fontSize: 14,
    width: 300,
    marginLeft: 10,
    fontFamily: 'Inter-Regular',
    alignSelf: 'center',
  },

  subTextStyle: {
    color: '#7F7F92',
    fontSize: 14,
    width: 300,
    marginTop: 10,
    fontFamily: 'Inter-Regular',
    alignSelf: 'center',
    textDecorationLine: 'underline',
  },
  checkBlock: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    //  marginLeft: 16,
    marginBottom: 82,
    justifyContent: 'flex-start',
  },
  checkbox: {
    width: 16,
    height: 16,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'blue',
  },
  labelText: {
    fontSize: 14,
    lineHeight: 21,
  },
  enterText: {
    width: 360,
    fontFamily: 'Inter-Regular',
    fontSize: 16,
    lineHeight: 24,
    color: '#1D1D1E',
    marginBottom: 40,
  },
  text: {
    color: '#aaa',
  },
  upperContainer: {
    backgroundColor: '#fff',
  },
  container: {
    paddingHorizontal: 16,
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
  },
  header: {
    marginVertical: 16,
    fontFamily: 'Inter-SemiBold',
    fontSize: 26,
    marginBottom: 30,
  },
  homeContent: {
    paddingHorizontal: 6,
  },
  button: {
    paddingHorizontal: 6,
    marginTop: 20,
  },
});
