import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Platform,
} from 'react-native';
import Button from '../../components/Button';
import Student from '../../assets/icons/student.svg';
import OrangeArrow from '../../assets/icons/OrangeArrow.svg';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { whatsappUrlHandler } from '../../utils/links';
import { handleLink } from '../../utils/links';
import { goToPaymentLink } from '../../utils/links';
import { TableContent } from '../../api/settings';
import { Profileblocks } from '../../utils/ProfileLinks';
import AuthUserComponent from '../../components/AuthUserComponent';
import ProfileModalComponent from '../../components/ProfileModalComponent';
import HouseIcon from '../../assets/icons/HouseIcon.svg';
import api from '../../api';
import ProfileLinksBlock from '../../components/ProfileLinksBlock';
import GoToPayComponent from '../../components/GoToPayComponent';
import YourTeacherComponent from '../../components/YourTeacherComponent';
import TimeTableComponent from '../../components/TimeTableComponent';
import { convertPhoneNumber, phoneNormalize } from '../../utils/phone';
export default function ProfileUserScreen({
  navigation
}) {
  const [modalVisible, setModalVisible] = useState(false);
  const [pupils, setPupils] = useState<TableContent['Ученики'][]>([]);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [users, setUsers] = useState<TableContent['Ученики'][]>([]);
  const [groups, setGroups] = useState<TableContent['Группы'][]>([]);
  const [staffPhone, setStaffPhone] = useState<TableContent['Телефон'][]>([]);
  useEffect(() => {
    const getDataFromStorage = async () => {
      try {
        const data = await AsyncStorage.getItem('phoneNumber');
        if (data !== null) {
          setPhoneNumber(data);
        } else {
          setPhoneNumber('');
        }
      } catch (error) {
        setPhoneNumber('');
      }
    };
    getDataFromStorage();
  }, []);
  const phoneNumberModified = phoneNormalize(phoneNumber);
  const phoneNumberForTable = convertPhoneNumber(phoneNumber);
  async function getUsers() {
    const users = (await api.readTable('Ученики')) || [];
    if (users) {
      setUsers(users);
      const filterUsers = users.filter(
        (item) => item.Телефон === phoneNumberForTable
      );
      setPupils(filterUsers);
    }
  }

  async function getStuffPhones() {
    try {
      const staffPhones = (await api.readTable('Телефон')) || [];
      if (staffPhones) {
        setStaffPhone(staffPhones)
      }
    } catch (error) {
      console.log('ERROR')
    }
  }

  const managersPhone = staffPhone[0]?.Телефон

  useEffect(() => {
    getUsers();
    getStuffPhones();
  }, []);

  async function getGroups() {
    const groups = (await api.readTable('Группы')) || [];
    if (groups) {
      setGroups(groups);
    }
  }

  useEffect(() => {
    getGroups();
  }, []);

  const openModal = () => {
    setModalVisible(true);
  };
  const closeModal = () => {
    setModalVisible(false);
  };

  const user = users.find((user) => user.Телефон === phoneNumberForTable);
  const name = user?.Родитель || '';
  const uri = user?.Аватарка || '';

  const filterUsers = users.filter(
    (item) => item.Телефон === phoneNumberForTable
  );

  const teatcher = (item) => {
    const groupOfPupil = item?.Группа;
    const foundGroup = groups.find((group) => group?.Группа === groupOfPupil);
    return foundGroup;
  };

  function onMenuPress(item: any) {
    if (
      item.text === 'Контакты English Centre' ||
      item.text === 'Политика конфиденциальности' 
  
    )  {
      handleLink(item.link);
    }
    else{
    if (item.text === 'Написать менеджеру центра') {
    handleLink(whatsappUrlHandler(managersPhone))
    }
    
     else  {
      if (phoneNumber){
      navigation.navigate(item.link,{ phoneNumberModified, name })}
      else {
        navigation.navigate('Authorization')
      }
    }}
  }

  return (
    <SafeAreaView style={styles.upperContainer}>
      <Text style={styles.header}>Профиль</Text>
      <ScrollView style={styles.keyBoardStyle}>
        {!phoneNumber && (
          <>
            <View style={styles.container}>
              {/* -----------------------неавторизованный юзер не удалять!!!!------------------ */}
              <View style={styles.homeIconWrapper}>
                <HouseIcon />
              </View>
              <Text style={styles.inviteText}>
                Войдите в приложение, чтобы видеть информацию об ученике
              </Text>
              <View style={styles.button}>
                <Button
                  backgroundColor={'#E73F1A'}
                  textColor="#fff"
                  text="Войти"
                  onPress={() => navigation.navigate('Authorization')}
                  marginBottom={4}
                  marginTop={16}
                />
              </View>

              <View style={styles.notYetPupilBlock}>
                <View style={styles.pupilBlock}>
                  <Student />
                  <Text style={styles.pupilText}>
                    Ещё не являетесь учеником English Centre?
                  </Text>
                </View>
                <TouchableOpacity onPress={openModal} style={styles.joinBlock}>
                  <Text style={styles.joinText}>Присоединиться</Text>
                  <View style={styles.OrangeArrowWrapper}>
                    <OrangeArrow />
                  </View>
                </TouchableOpacity>
              </View>
              {/* -------------------------------------------------------------- */}

              <ProfileModalComponent
                whatsAppOnPress={() => handleLink(whatsappUrlHandler(managersPhone))}
                onRequestClose={closeModal}
                onPress={closeModal}
                visible={modalVisible}
              />
            </View>
          </>
        )}
        {phoneNumber && (
          <>
            <View style={styles.userHeader}>
              <AuthUserComponent
                imageUrl={uri}
                userName={name}
                userPhone={phoneNumberModified}
              />
            </View>
            <View style={styles.userContainer}>
              <GoToPayComponent onPress={() => handleLink(goToPaymentLink)} />
              <View>
                <Text style={styles.yourTeacherText}>Ваш учитель</Text>
                {filterUsers.map((pupil, i) => (
                  <YourTeacherComponent
                    key={i}
                    group={teatcher(pupil)}
                    pupil={pupil?.Ученик}
                    count={filterUsers.length}
                  />
                ))}
              </View>
              <Text style={styles.pupilsTableText}>Расписание ученика</Text>

              {filterUsers.map((pupil, i) => (
                <TimeTableComponent
                  key={i}
                  group={teatcher(pupil)}
                  pupil={pupil?.Ученик}
                  individual={pupil['Индивидуальные занятия']}
                  count={filterUsers.length}
                />
              ))}
            </View>
          </>
        )}
        <View style={styles.userContainer}>
          <View style={styles.greyLine} />
          <View style={styles.profileBlockWrapper}>
            {Profileblocks.map((item) => (
              <ProfileLinksBlock
                key={item.id}
                text={item.text}
                onPress={() => onMenuPress(item)}
                isLast={item.id === 44}
              />
            ))}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
 
  yourTeacherText: {
    color: '#1D1D1E',
    fontSize: 14,
    fontFamily: 'Inter-SemiBold',
    textTransform: 'uppercase',
    marginBottom: 16,
  },
  pupilsTableText: {
    color: '#1D1D1E',
    fontSize: 14,
    fontFamily: 'Inter-SemiBold',
    textTransform: 'uppercase',
    marginBottom: 16,
    marginTop: 25,
  },

  cont: {
    width: '100%',
    height: 90,
  },

  profileBlockWrapper: {
    borderRadius: 16,
    backgroundColor: '#fff',
    width: '100%',
    alignSelf: 'center',
    paddingTop: 18,
    marginBottom: 60,
  },
  
  profileLinksBlock: {
    paddingTop: 20,
    paddingHorizontal: 16,
    width: '88%',
    backgroundColor: '#fff',
    alignSelf: 'center',
    borderRadius: 16,
  },
  greyLine: {
    width: '100%',
    height: 0.2,
    alignSelf: 'center',
    backgroundColor: '#BFBFC8',
    marginBottom: 28,
  },
  inviteText: {
    color: '#1D1D1E',
    fontSize: 16,
    lineHeight: 24,
    fontFamily: 'Inter-Regular',
    textAlign: 'center',
  },
  homeIconWrapper: {
    alignSelf: 'center',
    marginBottom: 20,
  },
 
  keyBoardStyle: {
    height: '100%',
    width: '100%',
    backgroundColor: '#F6F6F6',
  },

  OrangeArrowWrapper: {
    alignSelf: 'center',
    marginLeft: 6,
  },
  joinText: {
    color: '#E73F1A',
    fontSize: 14,
    fontFamily: 'Inter-SemiBold',
    marginLeft: 55,
  },
  joinBlock: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  pupilText: {
    width: 200,
    fontSize: 14,
    color: '#1D1D1E',
    fontFamily: 'Inter-Regular',
    lineHeight: 20,
    alignSelf: 'center',
    marginLeft: 16,
  },
  pupilBlock: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 10,
  },

  notYetPupilBlock: {
    width: '96%',
    paddingTop: 20,
    paddingLeft: 20,
    paddingBottom: 20,
    backgroundColor: '#F5F5F7',
    borderColor: '#EAEAEA',
    borderRadius: 16,
    alignSelf: 'center',
    borderWidth: 1,
  },

  text: {
    color: '#aaa',
  },
  upperContainer: {
    backgroundColor: '#fff',
    flex: 1,
  },
  container: {
    paddingHorizontal: 16,
    width: '100%',
    backgroundColor: '#fff',
    paddingBottom: 40,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    marginBottom: 28,
  },
  userHeader: {
    paddingHorizontal: 16,
    width: '100%',
    backgroundColor: '#fff',
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    marginBottom: 20,
  },
  userContainer: {
    paddingHorizontal: 16,
  },
  header: {
    marginTop: Platform.OS === 'ios' ? 4 : 34,
    marginLeft: 16,
    fontFamily: 'Inter-SemiBold',
    fontSize: 22,
    marginBottom: 10,
    lineHeight: 32,
  },
  button: {
    paddingHorizontal: 6,
    marginBottom: 34,
  },
});
