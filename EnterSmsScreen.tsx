import React, { useContext, useEffect, useState, useRef } from 'react';
import { RouteProp, useRoute } from '@react-navigation/native';
import Countdown from '../../components/CountdownTimer';
import {
  View,
  Text,
  SafeAreaView,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Platform
} from 'react-native';
import Button from '../../components/Button';
import InputField from '../../components/InputField';
import { NavigationProps } from '../../navigation';
import { phoneNormalize } from '../../utils/phone';
import { convertPhoneNumber } from '../../utils/phone';
import { getApp, initializeApp } from 'firebase/app';

import GoBackArrow from '../../assets/icons/GoBackArrow.svg';
import {
  FirebaseRecaptchaVerifierModal,
  FirebaseRecaptchaBanner,
} from 'expo-firebase-recaptcha';
import {
  getAuth,
  PhoneAuthProvider,
  signInWithCredential,
} from 'firebase/auth';
import firebaseConfig from '../../../config/firebase';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as firebase from 'firebase/app';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

type RootStackParamList = {
  EnterSmsScreen: {
    firebasePhoneNumber: string;
    phoneNumber: string;
    verificationId: string;
    info: string;
  };
};
type EnterSmsScreenRouteProp = RouteProp<RootStackParamList, 'EnterSms'>;

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
try {
  firebase.initializeApp(firebaseConfig);
} catch (error) {
  console.log('Initializing error ', error);
}

if (!app?.options || Platform.OS === 'web') {
  throw new Error(
    'This example only works on Android or iOS, and requires a valid Firebase config.'
  );
}

export default function EnterSmsScreen({
  navigation,
}: NavigationProps<'Profile', 'Authorization'>) {
  const route = useRoute<EnterSmsScreenRouteProp>();
  const { firebasePhoneNumber, verificationId, info, phoneNumber } =
    route.params;

  const recaptchaVerifier = useRef(null);
  const [code, setVerificationCode] = useState('');
  const fbConfig = app ? app.options : undefined;
  const [countdown, setCountdown] = useState(60);
  const [countdownDisplay, setCountDownDisplay] = useState(false);
  const [isRightCode, setIsRightCode] = useState(true);
  const [repeatedVerificationId, setrepeatedVerificationId] = useState('');

  //------ввод и отправка номера телефона для получения СМС----------
  const handleSendVerificationCode = async () => {
    try {
      const phoneProvider = new PhoneAuthProvider(auth);
      const newVerificationId = await phoneProvider.verifyPhoneNumber(
        firebasePhoneNumber,
        // '+7 911 555-34-34',
        recaptchaVerifier.current
      ); // get the verification id
      setrepeatedVerificationId(newVerificationId);
      setCountDownDisplay(!countdownDisplay);

    } catch (error) {
      console.log(error);
    }
  };

  //------------AsyncStorage-----------------------------

  const savePhoneNumber = async (phoneNumber: string) => {
    try {
      await AsyncStorage.setItem('phoneNumber', phoneNumber);
    } catch (error) {
    }
  };

  const updateCountdown = (newCountdown: any) => {
    setCountdown(newCountdown);
  };
  //-----отправка введенного кода на firebase
  const handleVerifyVerificationCode = async (
    code,
    verificationId,
    repeatedVerificationId
  ) => {
    try {
      setIsRightCode(true);
      if (code.length === 6) {
        const credential = PhoneAuthProvider.credential(
          repeatedVerificationId || verificationId,
          code
        );
        await signInWithCredential(auth, credential);

        // Save the phone number to AsyncStorage
        savePhoneNumber(firebasePhoneNumber);
        navigation.navigate('Home');
      }
    } catch (error) {
      setIsRightCode(false);
    }
  };

  const borderColorHandler = (): string => {
    if (code.length === 6 && !isRightCode) {
      return '#E51717';
    } else if (code.length > 0) {
      return '#F89235';
    } else if (code.length === 6 && !isRightCode) {
      return '#E51717';
    } else {
      return '#fff';
    }
  };

  useEffect(() => {
    if (countdownDisplay) {
      setCountdown(60);
    }
  }, [countdownDisplay]);

  return (
    <SafeAreaView style={styles.upperContainer}>
      <KeyboardAwareScrollView style={styles.keyBoardStyle}>
        <View style={styles.container}>
          <FirebaseRecaptchaVerifierModal
            ref={recaptchaVerifier}
            firebaseConfig={fbConfig}
          />
          <View style={styles.homeContent}>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={styles.goBack}
            >
              <GoBackArrow />
            </TouchableOpacity>
            <Text style={styles.headerText}>Введите полученный код из СМС</Text>

            <View style={styles.phoneTextWrapper}>
              <Text
                style={styles.sendNumberTextText}
              >{`Мы отправили его на номер`}</Text>
              <Text style={styles.phoneNumberText}>{`${phoneNumber}`}</Text>
            </View>

            <InputField
              borderWidth={1}
              borderColor={borderColorHandler()}
              placeholder="Введите код"
              onChangeValue={(code) => {
                handleVerifyVerificationCode(
                  code,
                  verificationId,
                  repeatedVerificationId
                ),
                  setVerificationCode(code);
              }}
            />

            {countdown === 0 ? (
              <Text style={styles.wrongCodeText}>
                Время действия кода истекло
              </Text>
            ) : code.length === 6 && !isRightCode ? (
              <Text style={styles.wrongCodeText}>Неверный код</Text>
            ) : null}

            {countdown < 1 ? (
              <Button
                onPress={() => handleSendVerificationCode()}
                backgroundColor={'#F89235'}
                text={'Отправить код повторно'}
                textColor="#fff"
                marginTop={20}
              />
            ) : (
              <Countdown
                countdown={countdown}
                updateCountdown={updateCountdown}
              />
            )}
          </View>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  phoneTextWrapper: {
    width: '100%',
    paddingHorizontal: 6,
    marginBottom: 12,
  },
  wrongCodeText: {
    paddingHorizontal: 9,
    // marginTop: -6,
    // marginBottom: 16,
    color: '#E51717',
    fontSize: 14,
    fontFamily: 'Inter-Regular',
  },
  headerText: {
    marginVertical: 24,
    paddingHorizontal: 6,
    fontFamily: 'Inter-SemiBold',
    fontSize: 26,
    marginBottom: 30,
  },
  sendNumberTextText: {
    color: '#1D1D1E',
    fontSize: 16,
    fontFamily: 'Inter-Regular',
    marginBottom: 7,
  },
  phoneNumberText: {
    color: '#1D1D1E',
    fontSize: 16,
    fontFamily: 'Inter-SemiBold',
    marginBottom: 16,
  },

  goBack: {
    paddingHorizontal: 6,
    marginTop: 30,
  },
  inActiveButton: {
    alignItems: 'center',
    padding: 19,
    marginTop: 16,
    marginBottom: 16,
    borderRadius: 26,
    backgroundColor: '#7F7F92',
  },
  inActiveText: {
    fontSize: 16,
    fontFamily: 'Inter-SemiBold',
    color: '#fff',
  },
  alertText: {
    color: '#E51717',
    fontSize: 14,
    fontFamily: 'Inter-Regular',
  },
  closeIconStyle: {
    position: 'absolute',
    top: 50,
    right: 30,
  },
  checkIconStyle: {
    position: 'absolute',
    top: 45,
    right: 25,
  },

  keyBoardStyle: {
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
  },

  labelText: {
    fontSize: 14,
    lineHeight: 21,
  },
  enterText: {
    paddingHorizontal: 16,
    width: 360,
    fontFamily: 'Inter-Regular',
    fontSize: 16,
    lineHeight: 24,
    color: '#1D1D1E',
    marginBottom: 40,
  },
  text: {
    color: '#aaa',
  },
  upperContainer: {
    backgroundColor: '#fff',
  },
  container: {
    paddingHorizontal: 16,
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
  },

  homeContent: {
    // marginTop: 32,
    // flex: 1,
  },
  button: {
    // paddingHorizontal: 6,
    // marginBottom: 24,
  },
});
