// https://www.regextester.com/99415
export const phoneValidator = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;


export const phoneNormalize = (phoneNumber: string) => {
  const cleanedNumber = phoneNumber.replace(/\D/g, '');

  let formattedNumber = '+7';

  if (cleanedNumber.length > 1) {
    formattedNumber += ' ' + cleanedNumber.slice(1, 4);

    if (cleanedNumber.length >= 5) {
      formattedNumber += ' ' + cleanedNumber.slice(4, 7);
    }

    if (cleanedNumber.length >= 8) {
      formattedNumber += '-' + cleanedNumber.slice(7, 9);
    }

    if (cleanedNumber.length >= 10) {
      formattedNumber += '-' + cleanedNumber.slice(9);
    }
  }

  return formattedNumber;
};


export const convertPhoneNumber = (formattedNumber: string) =>{
  const cleanedNumber = formattedNumber.replace(/\D/g, "");
  if (cleanedNumber.startsWith("7")) {
    return "8" + cleanedNumber.slice(1);
  }
  return cleanedNumber;
}



export const toFirebasePhoneNumber = (phoneNumber: string)=> {
  const cleanedNumber = phoneNumber.replace(/\D/g, "");

  if (cleanedNumber.startsWith("7")) {
    return "+7" + cleanedNumber.slice(1);
  }
  if (cleanedNumber.startsWith("8")) {
    return "+7" + cleanedNumber.slice(1);
  }

  return cleanedNumber;
}