import { toConfidentialityPolicyLink, toContacts, toWatsappLink } from "./links";

interface ProfileBlock {
  id: number;
  text: string;
  link?: string;
}

export const Profileblocks: ProfileBlock[] = [
  {
    id: 11,
    text: 'Написать менеджеру центра',
  },
  {
    id: 22,
    text: 'Контакты English Centre',
    link: toContacts,
  },
  {
    id: 33,
    text: 'Политика конфиденциальности',
    link: toConfidentialityPolicyLink,
  },
  {
    id: 44,
    text: 'Аккаунт пользователя',
    link: 'AccountUser',
  },
];

